<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" href="css/login.css">


    <link rel="stylesheet" href="css/index.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">


</head>
<body>

<div class="topnav">
    <?php
    include 'navbar/show_navbar.php';
    showNavBar();
    ?>
</div>
<div id="main">



    <form id="loginForm" method="post">
        <div class="imgcontainer">
            <img src="./resources/loginicon.jpg" alt="Avatar" class="avatar">
        </div>

        <div>
            <p id="error"></p>
        </div>

        <div class="container">
            <div>
                <label for="user"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="user" id ="user" required>
            </div>


            <div>
                <label for="psw"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="psw" id ="psw" required>
            </div>


            <div>

                <button type="button" id="login">Login</button>
            </div>

            <div>
                <label>
                    <input type="checkbox" checked="checked" name="remember"> Remember me
                </label>
            </div>

        </div>


    </form>
</div>

<script type="text/javascript" charset="utf-8" src="js/login.js"></script>

</body>
</html>
