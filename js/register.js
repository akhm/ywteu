$('#register').click(function() {

    if(isValidateForm()) {

        $.ajax({
            type: 'POST',
            url: 'services/register.php',
            data: $("#registerForm").serialize(),
            dataType: 'json',
            success: function (data) {
                if (data['status']) {
                    window.location.replace("http://localhost/webbahar");
                } else {
                    alert(data['msg']);
                }
            }
        });
    }
});


function isValidateForm(){

    if($('#name').val().length < 3){
        showError('fill name by min length 3');
        return false;
    }else if($('#surename').val().length < 3){
        showError('fill surename by min length 3');
        return false;
    }else if($('#adress').val().length < 5){
        showError('fill adress by min length 5');
        return false;
    }else if($('#postalcode').val().length < 8){
        showError('fill postalcode by min length 8');
        return false;
    }else if($('#ort').val().length < 3){
        showError('fill ort by min length 3');
        return false;
    }else if($('#emailadress').val().length < 3 ){
        showError('invalidate email address');
        return false;
    }else if($('#username').val().length < 3){
        showError('invalidate username');
        return false;
    }else if($('#password').val().length < 4){
        showError('invalidate password');
        return false;
    }else if($('#repassword').val().length < 4){
        showError('invalidate repassword');
        return false;
    }else if($('#repassword').val() != $('#password').val()){
        showError("password and repassword is not same!")
        return false;
    }else{
        return true;
    }

}




function showError(msg) {
    // Get the snackbar DIV
    var x = document.getElementById("snackbar");

    // Add the "show" class to DIV
    x.className = "show";

    x.innerHTML = msg;

    // After 3 seconds, remove the show class from DIV
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}
