$( document ).ready(function() {
    console.log( "ready!" );
});


function reserveProduct(productId) {

    $.ajax({
        type: 'POST',
        url: 'services/product_reserve.php',
        data: {id:productId},
        dataType: 'json',
        success: function (data) {
            if(data){
                showSnackBar("success");
            }else{
                showSnackBar("failed");
            }
        }
    });
}



function showSnackBar(msg) {
    // Get the snackbar DIV
    var x = document.getElementById("snackbar");

    // Add the "show" class to DIV
    x.className = "show";

    x.innerHTML = msg;

    // After 3 seconds, remove the show class from DIV
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}




