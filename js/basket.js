$( document ).ready(function() {

    $.ajax({
        type: 'POST',
        url: 'services/basket.php',
        data: 3,
        dataType: 'json',
        success: function (data) {
            if(data['status']){
                showTable(data['result']);
            }else{
                showSnackBar("failed load page!");
            }
        }
    });

});


function showTable(products) {

    var tb = "<table style=\"width:100%\">" +
    "<tr>"  +
    "<th>id</th>"+
    "<th>Name</th>"+
    "<th>Count</th>"+
    "<th>Price unit</th>"+
    "<th>Price sum</th>"+
    "</tr>";


    var sumAll = 0;
    var countAll = 0;


    for(var i = 0 ; i < products.length ; i++){
        var p = products[i];
        tb +=
        "<tr>" +
        "<td>" + (i+1) + "</td>" +
        "<td>" + p['name'] + "</td>" +
        "<td>" + p['count'] + "</td>" +
        "<td>" + p['price'] + "</td>" +
        "<td>" + (p['price'] * p['count']) + "</td>" +
        "</tr>";


        sumAll += parseInt(p['price']);
        countAll += parseInt(p['count']);
    }


    tb += "</table>";


    tb += "<table style=\"width:100%\">" +
        "<tr>"  +
        "<th>Count All</th>"+
        "<th>All Price</th>"+
        "</tr>";

    tb += "<tr>" +
        "<td>" + countAll + "</td>" +
        "<td>" + sumAll + "</td>" +
        "</tr>";

    tb += "</table>";


    $('#tableBasket').html(tb);


}



function showSnackBar(msg) {
    // Get the snackbar DIV
    var x = document.getElementById("snackbar");

    // Add the "show" class to DIV
    x.className = "show";

    x.innerHTML = msg;

    // After 3 seconds, remove the show class from DIV
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}
