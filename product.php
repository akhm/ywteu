<!DOCTYPE html>
<html>
<head>
    <title>Product</title>
    <link rel="stylesheet" href="css/product.css">
    <link rel="stylesheet" href="css/index.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

</head>
<body>

<div class="topnav">
    <?php
    include 'navbar/show_navbar.php';
    showNavBar();
    ?>
</div>
<div id="main">

        <p style="font-size: 50px; text-align: center;"> OUR PRODUCTS </p>
    <hr>
        <p style="font-size: 30px; text-align: center;"> Here you can see our Products and shop
        online </p>

        <?php

                include 'services/product.php';
                echo
                "<table style=\"width:100%\">
                    <tr>
                        <th>id</th>
                        <th>image</th>
                        <th>Name</th>
                        <th>Rate</th>
                        <th>Price</th>
                        <th>reserve</th>
                    </tr>";
            $users = json_decode(getAllProduct() , true);
            for($i = 0 ; $i < count($users) ; $i++){
                $user = $users[$i];
                    echo
                    "<tr>
                        <td>{$user["id"]}</td>
                        <td> <img src=\"{$user["image"]}\" alt=\"Smiley face\" height=\"42\" width=\"42\"> </img></td>
                        <td>{$user["name"]}</td>
                        <td>{$user["rate"]}</td>
                        <td>{$user["price"]}</td>
                        <td><input type='button' onclick='reserveProduct({$user["id"]})'></td>
                    </tr>";
            }
            echo "</table>";


        ?>

    <div id="snackbar">Some text some message..</div>


</div>

<script type="text/javascript" charset="utf-8" src="js/product.js"></script>

</body>
</html>

