<?php
/**
 * Created by PhpStorm.
 * User: morteza
 * Date: 6/8/18
 * Time: 6:33 AM
 */

    include '../util/user_util.php';

    function getNavBar($role){

        if($role == null){
            return defaultNavBar();
        }else if($role == "user"){
            return userNavBar();
        }else if($role == "admin") {
            return adminNavBar();

        }

    }


    function defaultNavBar(){
        return getMenuByRole("no");
    }

    function userNavBar(){
        return getMenuByRole("user");
    }

    function adminNavBar(){
        return getMenuByRole("admin");
    }



    function getMenuByRole($roleName){
        $menus = [];
        foreach (getNavBarData() as $head){

            if(strpos($head->role, $roleName) !== false){
                $menus[] = $head;
            }
        }

        return $menus;
    }

    function getMenu(){
        return getMenuByRole(getUserRole());
    }

    function getNavBarData(){

        $data = json_decode(file_get_contents("navbar.json" , true));


        return $data->headers;

    }






?>