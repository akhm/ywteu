<?php
error_reporting(E_ERROR | E_PARSE);
include './util/user_util.php';
include 'model/User.php';
include 'navbar/navBar.php';
include 'util/navBar.php';
include 'util/server_util.php';

    function showNavBar(){

        $user = getUser();

        if($user != null) {
            echo
                "<a " .
                "class=\"profile_nav\" " .
                "active=\"" . ("false") . "\" " .
                "href=\"" . "account.php" .
                "\">" .
                $user -> getName() .
                "</a>";
        }else{
            echo
            "<a " .
            "class=\"profile_nav\"" .
                "active=\"" . ("false") . "\" " .
                "href=\"" . "register.php" .
                "\">" .
                "Guest" .
                "</a>";
        }

        foreach (getMenu() as $m){
            $anchorText = $m->icon !== null ? '<img src="'.$m->icon.'" class="basket_icon" />' : $m->name;
            echo
                "<a " .
                "active=\"" . ("false") . "\" " .
                "href=\"". $m->url .
                "\">".
                $anchorText .
                "</a>";
        }
    }

?>