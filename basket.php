<!DOCTYPE html>
<html>
<head>
    <title>Basket</title>
    <link rel="stylesheet" href="css/basket.css">
    <link rel="stylesheet" href="css/index.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>

</head>
<body>

<div class="topnav">
    <?php
    include 'navbar/show_navbar.php';
    showNavBar();
    ?>
</div>
<div id="main">



    <div id="tableBasket">Loading please wait</div>


    <div id="snackbar">Some text some message..</div>


</div>

<script type="text/javascript" charset="utf-8" src="js/basket.js"></script>

</body>
</html>

