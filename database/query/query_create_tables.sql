CREATE TABLE IF NOT EXISTS `shop_bahar`.`users` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(20) NOT NULL , `surename` VARCHAR(25) NOT NULL , `adress` VARCHAR(50) NOT NULL , `postalcode` VARCHAR(15) NOT NULL , `ort` VARCHAR(15) NOT NULL , `emailadress` VARCHAR(30) NOT NULL , `username` VARCHAR(25) NOT NULL , `password` VARCHAR(100) NOT NULL , `rememberToken` VARCHAR(40) NOT NULL , `role` VARCHAR(40) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB

CREATE TABLE IF NOT EXISTS `shop_bahar`.`product` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(25) NOT NULL , `image` VARCHAR(50) NOT NULL , `cost` INT NOT NULL , `rate` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `shop_bahar`.`category` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(25) NOT NULL , `image` VARCHAR(50) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `shop_bahar`.`product_category` ( `id` INT NOT NULL AUTO_INCREMENT , `category_id` INT NOT NULL , `product_id` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `shop_bahar`.`user_bascket` ( `id` INT NOT NULL AUTO_INCREMENT , `reserved` BOOLEAN NOT NULL , `product_id` INT NOT NULL , `user_id` INT NOT NULL , `order_date` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;

INSERT INTO `users`(`name`, `surename`, `adress`, `postalcode`, `ort`, `emailadress`, `username`, `password`, `rememberToken`, `role`) VALUES ('bahar','bahar','bahar','bahar','bahar','bahar','bahar',md5('bahar'),'bahar','admin')