<!DOCTYPE html>
<html>
<head>
    <title>Admin Manage Product</title>
    <link rel="stylesheet" href="css/manageProduct.css">
    <link rel="stylesheet" href="css/index.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

</head>
<body>

<div class="topnav">
    <?php
    include 'navbar/show_navbar.php';
    showNavBar();
    ?>
</div>
<div id="main">



        <?php

                include 'services/product.php';
                echo
                "<table style=\"width:100%\">
                    <tr>
                        <th>id</th>
                        <th>image</th>
                        <th>Name</th>
                        <th>Rate</th>
                        <th>Price</th>
                        <th>Delete</th>
                    </tr>";
            $users = json_decode(getAllProduct() , true);
            for($i = 0 ; $i < count($users) ; $i++){
                $user = $users[$i];
                    echo
                    "<tr>
                        <td>{$user["id"]}</td>
                        <td> <img src=\"{$user["image"]}\" alt=\"Smiley face\" height=\"42\" width=\"42\"> </img></td>
                        <td>{$user["name"]}</td>
                        <td>{$user["rate"]}</td>
                        <td>{$user["price"]}</td>
                        <td><input type='button' onclick='deleteProduct({$user["id"]})'></td>
                    </tr>";
            }
            echo "</table>";


        ?>

    <div>
        <input type="button" id="addProduct" value="Add New Product"/>
    </div>


</div>

<script type="text/javascript" charset="utf-8" src="js/manageProduct.js"></script>

</body>
</html>

