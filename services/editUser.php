<?php
/**
 * Created by PhpStorm.
 * User: morteza
 * Date: 6/8/18
 * Time: 5:45 PM
 */

    include '../model/User.php';


    function getAllUser(){


        $conn = getDatabaseConnection();


        $query =
                "SELECT * FROM `users` ";

            $result = $conn->query($query);

            $users = "[ ";

            if($result -> num_rows > 0) {

                while ($row = $result->fetch_assoc()) {

                    $users = $users .
                        "{\"id\":" . $row["id"] . " , \"name\": \"" . $row["name"] . "\" , \"role\": \"" . $row["role"] . "\"}";

                }

            }

            $users = $users ."]";



            $conn->close();

            return $users;

    }


    function getDatabaseConnection(){

        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "shop_bahar";

        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }


        return$conn;
    }



?>