<?php
/**
 * Created by PhpStorm.
 * User: morteza
 * Date: 6/8/18
 * Time: 5:45 PM
 */



    function getAllProduct(){


        $conn = getDatabaseConnection();


        $query =
                "SELECT * FROM `product` ";

            $result = $conn->query($query);

            $products = "[ ";

            $ok = false;

            if($result -> num_rows > 0) {

                while ($row = $result->fetch_assoc()) {

                    if($ok){
                        $products = $products . " , ";
                    }
                    $ok = true;
                    $products = $products . "{".
                        "\"id\" : " . $row["id"] . " , ".
                        "\"name\" : \"" . $row["name"] . "\" , ".
                        "\"image\" : \"" . $row["image"] . "\" , ".
                        "\"rate\" : " . $row["rate"] . " , ".
                        "\"price\" : " . $row["cost"] .
                        "}";

                }

            }

            $products = $products ." ]";

//            echo $products;


            $conn->close();

            return $products;

    }


    function getDatabaseConnection(){

        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "shop_bahar";

        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }


        return$conn;
    }



?>