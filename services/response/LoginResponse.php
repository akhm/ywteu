<?php
/**
 * Created by PhpStorm.
 * User: morteza
 * Date: 6/8/18
 * Time: 9:50 AM
 */

class LoginResponse
{

    public $status;

    public $user;

    /**
     * LoginResponse constructor.
     * @param $status
     * @param $user
     */
    public function __construct($status, $user)
    {
        $this->status = $status;
        $this->user = $user;
    }


}