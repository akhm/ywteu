<?php
/**
 * Created by PhpStorm.
 * User: morteza
 * Date: 6/8/18
 * Time: 11:56 AM
 */

class RegisterResponse
{

    public $status;

    public $msg;

    public $result;

    /**
     * RegisterResponse constructor.
     * @param $status
     * @param $msg
     * @param $result
     */
    public function __construct($status, $msg, $result)
    {
        $this->status = $status;
        $this->msg = $msg;
        $this->result = $result;
    }


}