<?php
/**
 * Created by PhpStorm.
 * User: morteza
 * Date: 6/8/18
 * Time: 12:02 PM
 */

class RegisterRequest
{

    private $name;

    private $surename;

    private $adress;

    private $postalcode;

    private $ort;

    private $emailadress;

    private $username;

    private $password;

    private $repassword;

    /**
     * RegisterRequest constructor.
     * @param $name
     * @param $surename
     * @param $adress
     * @param $postalcode
     * @param $ort
     * @param $emailadress
     * @param $username
     * @param $password
     * @param $repassword
     */
    public function __construct($name, $surename, $adress, $postalcode, $ort, $emailadress, $username, $password, $repassword)
    {
        $this->name = $name;
        $this->surename = $surename;
        $this->adress = $adress;
        $this->postalcode = $postalcode;
        $this->ort = $ort;
        $this->emailadress = $emailadress;
        $this->username = $username;
        $this->password = $password;
        $this->repassword = $repassword;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getSurename()
    {
        return $this->surename;
    }

    /**
     * @return mixed
     */
    public function getAdress()
    {
        return $this->adress;
    }

    /**
     * @return mixed
     */
    public function getPostalcode()
    {
        return $this->postalcode;
    }

    /**
     * @return mixed
     */
    public function getOrt()
    {
        return $this->ort;
    }

    /**
     * @return mixed
     */
    public function getEmailadress()
    {
        return $this->emailadress;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getRepassword()
    {
        return $this->repassword;
    }





}