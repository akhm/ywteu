<?php
/**
 * Created by PhpStorm.
 * User: morteza
 * Date: 6/8/18
 * Time: 9:49 AM
 */
include '../util/user_util.php';
include 'response/RegisterResponse.php';
//include 'request/RegisterRequest.php';
include '../database/database_config.php';

    $response = new RegisterResponse(true , "filed is empty",null);




    $register = new RegisterRequest(
        $_POST['name'] ,
        $_POST['surename'] ,
        $_POST['adress'] ,
        $_POST['postalcode'] ,
        $_POST['ort'],
        $_POST['emailadress'] ,
        $_POST['username'] ,
        md5($_POST['password']) ,
        md5($_POST['repassword'])
    );

    if(!isset($_POST['name'])){
        $response->msg  = $response->msg . "name should be set" . "\n";
        $response->status = false;
    } else if(!isset($_POST['surename'])){
        $response->msg  = $response->msg . "surename should be set" . "\n";
        $response->status = false;

    }else if(!isset($_POST['adress'])){
        $response->msg  = $response->msg . "adress should be set" . "\n";
        $response->status = false;

    }else if(!isset($_POST['postalcode'])){
        $response->msg  = $response->msg . "postalcode should be set" . "\n";
        $response->status = false;

    }else if(!isset($_POST['ort'])){
        $response->msg  = $response->msg . "ort should be set" . "\n";
        $response->status = false;

    }else if(!isset($_POST['emailadress'])){
        $response->msg  = $response->msg . "Emailadress should be set" . "\n";
        $response->status = false;

    }else if(!isset($_POST['username'])){
        $response->msg  = $response->msg . "username should be set" . "\n";
        $response->status = false;

    }else if(!isset($_POST['password'])){
        $response->msg  = $response->msg . "password should be set" . "\n";
        $response->status = false;

    }else if(!isset($_POST['repassword'])){
        $response->msg  = $response->msg . "repassword should be set" . "\n";
        $response->status = false;

    }

    if($response->status) {

        if (strcmp($register->getPassword()  , $register->getRepassword()) != 0) {
            $response->status = false;
            $response->msg = "password and repassword is not same! {$register->getPassword()} , {$register->getRepassword()}";
        }else{
            $user = doRegister($register);
            $response->status = $user;
            $response -> msg = "success register";
//            $response -> result = $user;
//            login($user);
        }
    }

    echo json_encode($response);




function doRegister($user){

        $conn = getDatabaseConnection();

        $query =
            "INSERT
                INTO
                    `users`(
                        `name`,
                        `surename`,
                        `adress`,
                        `postalcode`,
                        `ort`,
                        `emailadress`,
                        `username`,
                        `password`,
                        `rememberToken`,
                        `role`
                    )
                VALUES(
                    \"{$user->getName()}\",
                    \"{$user->getSurename()}\",
                    \"{$user->getAdress()}\",
                    \"{$user->getPostalcode()}\",
                    \"{$user->getOrt()}\",
                    \"{$user->getEmailadress()}\",
                    \"{$user->getUsername()}\",
                    \"{$user->getPassword()}\",
                    \"--\",
                    \"user\")";

        $result = $conn->query($query);

        $conn->close();


        return $result;
    }


class RegisterRequest
{

    private $name;

    private $surename;

    private $adress;

    private $postalcode;

    private $ort;

    private $emailadress;

    private $username;

    private $password;

    private $repassword;

    /**
     * RegisterRequest constructor.
     * @param $name
     * @param $surename
     * @param $adress
     * @param $postalcode
     * @param $ort
     * @param $emailadress
     * @param $username
     * @param $password
     * @param $repassword
     */
    public function __construct($name, $surename, $adress, $postalcode, $ort, $emailadress, $username, $password, $repassword)
    {
        $this->name = $name;
        $this->surename = $surename;
        $this->adress = $adress;
        $this->postalcode = $postalcode;
        $this->ort = $ort;
        $this->emailadress = $emailadress;
        $this->username = $username;
        $this->password = $password;
        $this->repassword = $repassword;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getSurename()
    {
        return $this->surename;
    }

    /**
     * @return mixed
     */
    public function getAdress()
    {
        return $this->adress;
    }

    /**
     * @return mixed
     */
    public function getPostalcode()
    {
        return $this->postalcode;
    }

    /**
     * @return mixed
     */
    public function getOrt()
    {
        return $this->ort;
    }

    /**
     * @return mixed
     */
    public function getEmailadress()
    {
        return $this->emailadress;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getRepassword()
    {
        return $this->repassword;
    }





}

?>