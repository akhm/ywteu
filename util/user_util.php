<?php
include '../model/User.php';



    function getUser(){
        if (!isset($_SESSION)) {
            session_start();
        }

        if(isset( $_SESSION['user']) ){
            $u = $_SESSION['user'];
            return $u;
        }


        return null;
    }

    function isVerified(){
       if(getUser() != null){
           return 1;
       }else{
           return 0;
       }
    }

    function getUserRole(){
        if(isVerified()) {
            return getUser()->getRole();
        }else{
            return "no";
        }
    }

    function getUserId(){
        if(isVerified()) {
            return getUser()->getId();
        }else{
            return 0;
        }
    }


    function login($user){

        if (!isset($_SESSION)) {
            session_start();
        }
        $_SESSION['user'] = $user;
    }


?>