<?php
/**
 * Created by PhpStorm.
 * User: morteza
 * Date: 6/8/18
 * Time: 7:32 AM
 */

class BuyBasket
{
    private $products = [];

    /**
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param array $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }


}