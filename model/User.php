<?php

    class User{

        private $id;

        private $name;

        private $role;

        /**
         * User constructor.
         * @param $id
         * @param $name
         * @param $role
         */
        public function __construct($id, $name, $role)
        {
            $this->id = $id;
            $this->name = $name;
            $this->role = $role;
        }




        /**
         * @return mixed
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * @param mixed $id
         */
        public function setId($id)
        {
            $this->id = $id;
        }

        /**
         * @return mixed
         */
        public function getName()
        {
            return $this->name;
        }

        /**
         * @param mixed $name
         */
        public function setName($name)
        {
            $this->name = $name;
        }

        /**
         * @return mixed
         */
        public function getRole()
        {
            return $this->role;
        }

        /**
         * @param mixed $role
         */
        public function setRole($role)
        {
            $this->role = $role;
        }



        public function jsonSerialize() {
            return
                "{\"id\": " . $this->id . " , " .
                "\"name\": " . $this->name . " , " .
                "\"role\": " . $this->role . " }" ;
        }
    }

?>