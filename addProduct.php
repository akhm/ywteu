<!DOCTYPE html>
<html>
<head>
    <title>Add Product</title>
    <link rel="stylesheet" href="css/register.css">
    <link rel="stylesheet" href="css/index.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">



</head>
<body>

<div class="topnav">
    <?php
    include 'navbar/show_navbar.php';
    showNavBar();
    ?>
</div>
<div id="main">



    <form id="addProductForm" method="post">
        <div class="container">
            <h1>Add New Product</h1>
            <p>Please fill in this form to create an Product.</p>
            <hr>

            <div>
                <p id="error"></p>
            </div>

            <div id="name">
                <label for="name"><b>name</b></label>
                <input type="text" placeholder="Enter name" name="name" required>

            </div>


            <div id="price">

                <label for="cost"><b>Price</b></label>
                <input type="text" placeholder="Enter Price" name="price" required>
            </div>


            <div id="rate">

                <label for="rate"><b>Rate</b></label>
                <input type="number" placeholder="Rate" name="rate" required>
            </div>


            <div id="image">
                <label for="image"><b>image</b></label>
                <input  type="file" placeholder="Enter postalcode" name="postalcode" required>
            </div>


            <div id="category">
                <label for="ort"><b>ort</b></label>
                <input type="text" placeholder="Enter ort" name="ort" required>

            </div>


            <div>

            </div>
            <button type="button" class="registerbtn" id="addProduct">Add</button>
        </div>

        <div class="container signin">
            <p>Already have an account? <a href="http://localhost/webbahar/login.php">Login</a>.</p>
        </div>
    </form>
</div>


<script type="text/javascript" charset="utf-8" src="js/addProduct.js"></script>


</body>
</html>
