<!DOCTYPE html>
<html>
    <head>
        <title>Shop</title>
        <link rel="stylesheet" type="text/css" href="css/index.css">
        <script type="text/javascript" charset="utf-8" src="js/index.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    </head>
    <body>

        <div class="topnav">
            <?php
                include 'navbar/show_navbar.php';
                showNavBar();
            ?>
        </div>
        <p id="wel">WELCOME TO MY WEBSITE </p>

        <div id="header">
        </div>

        <footer id="foot">     <!--start of the  footer, we gave the section thr Id named thefooter ,so we can jump  to it whenever we click on the find us on the menu bar-->
            <div class="tabelle">
                <h4>Sozial Netze</h4>
                <ul>
                    <li> Facebook </li>
                    <li> >Twitter </li>
                    <li> Instagram </li>
                    <li>  Twitter </li>
                </ul>
            </div>
            <div class="tabelle">
                <h4>Kontakt</h4>
                <ul>
                    <li> Adresse </li>
                    <li> Webpr&auml;sense </li>
                    <li> Mail </li>
                    <li> Service-Hotline </li>

                </ul>
            </div>
            <div class="tabelle">
                <h4>Term of Use</h4>
                <ul>
                    <li>Copyright</li>
                    <li>Privacy Policy</li>
                </ul>
            </div>
            <div class="tabelle">
                <h4>About us</h4>
                <ul>
                    <li>Technikum Wien </li>
                    <li>Bachelor Wirtschaftsinformatik </li>
                    <li>1200 Wien </li>
                </ul>
            </div>
        </footer>  <!--End of the section-->

    </body>
</html> 

