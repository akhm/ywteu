<!DOCTYPE html>
<html>
<head>
    <title>Register</title>
    <link rel="stylesheet" href="css/register.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script type="text/javascript" src="js/jquery.min.js"></script>



</head>
<body>

<div class="topnav">
    <?php
    include 'navbar/show_navbar.php';
    showNavBar();
    ?>
</div>
<div id="main">



    <form id="registerForm" method="post">
        <div class="container">
            <h1 id="reg">Register</h1>
            <p id="loglog">Please fill in this form to create an account.</p>
            <hr>

            <div>
                <p id="error"></p>
            </div>

            <div >
                <label for="name"><b>Name</b></label>
                <input id="name" type="text" placeholder="Enter name" name="name" required>

            </div>


            <div >

                <label for="surename"><b>Surename</b></label>
                <input id="surename" type="text" placeholder="Enter surename" name="surename" required>
            </div>


            <div >

                <label for="adress"><b>Address</b></label>
                <input id="adress" type="text" placeholder="Enter adress" name="adress" required>
            </div>


            <div >
                <label for="postalcode"><b>Postalcode</b></label>
                <input id="postalcode" type="text" placeholder="Enter postalcode" name="postalcode" required>

            </div>


            <div>
                <label for="ort"><b>Ort</b></label>
                <input  id="ort" type="text" placeholder="Enter ort" name="ort" required>

            </div>

            <div >
                <label for="emailadress"><b>Emailadress</b></label>
                <input id="emailadress" type="email" placeholder="Enter emailadress" name="emailadress" required>

            </div>

            <div >
                <label for="username"><b>Username</b></label>
                <input id="username" type="text" placeholder="Enter username" name="username" required>

            </div>



            <div >
                <label for="password"><b>Password</b></label>
                <input id="password" type="password" placeholder="password" name="password" required>

            </div>

            <div >

                <label for="repassword"><b>Repassword</b></label>
                <input id="repassword" type="password" placeholder="repassword" name="repassword" required>
            </div>

            <div>

            </div>
            <button type="button" class="registerbtn" id="register" >Register</button>
        </div>

        <div class="container signin">
            <p>Already have an account? <a href="http://localhost/shop-morteza/login.php">Login</a>.</p>
        </div>
    </form>


    <div id="snackbar">Some text some message..</div>

</div>


<script type="text/javascript" charset="utf-8" src="js/register.js"></script>


</body>
</html>
